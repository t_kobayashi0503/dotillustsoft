﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DotIllustSoft
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public Color GetColor()
        {
            return colorSelectButton.BackColor;
        }

        public void Reset()
        {
            //パネル内ボタンを全て白にする
            for (int i = this.panel1.Controls.Count - 1; 0 <= i; i--)
            {
                this.dotButtons[i].BackColor = color;
            }
        }

        //ボタンコントロール配列のフィールドを作成
        private System.Windows.Forms.Button[] dotButtons;
        Color color = ColorTranslator.FromHtml("#FFFFFF");

        //フォームのLoadイベントハンドラ
        private void Form1_Load(object sender, System.EventArgs e)
        {
            //ボタンコントロール配列の作成
            this.dotButtons = new System.Windows.Forms.Button[256];

            //ボタンコントロールのインスタンス作成し、プロパティを設定する
            for (int i = 0; i < this.dotButtons.Length; i++)
            {
                //インスタンス作成
                this.dotButtons[i] = new System.Windows.Forms.Button();
                //プロパティ設定
                this.dotButtons[i].Name = "Button" + i.ToString();
                this.dotButtons[i].Size = new Size(19, 19);
                this.dotButtons[i].Location = new Point(i * 19);
                this.dotButtons[i].Margin = new Padding(0);
                this.dotButtons[i].BackColor = color;
                //イベントハンドラに関連付け
                this.dotButtons[i].Click +=
                    new EventHandler(this.DotButtonClick);
            }
            //FlowLayoutPanelにコントロールを追加
            this.panel1.Controls.AddRange(this.dotButtons);
            this.ResumeLayout(false);
        }


        //Buttonのクリックイベントハンドラ
        private void ColorButtonClicked(object sender, EventArgs e)
        {
            //カラーダイアログを表示する
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                colorSelectButton.BackColor = colorDialog.Color;
            }
        }

        private void DotButtonClick(object sender, EventArgs e)
        {
            Color color = this.GetColor();
            ((Button)sender).BackColor = color;
        }

        private void ResetButtonClicked(object sender, EventArgs e)
        {
            Reset();
        }
    }
}
